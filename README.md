# ecommerce-apis-assignment-django

## Steps to get started
1. Activate virtual environment ecomenv\Scripts\activate from this directory
2. Now move to project directory
   cd EcommerceApi
3. Now create the database and change the name of the database in settings.py
4. Now run "python manage.py makemigrations" to create migrations it will create the migrations or it will says no changes detected then move to next step
5. Now run "python manage.py migrate" to load all the tables in the database
6. Now run "python manage.py dummyusers" to create the dummy users. After running this successfully it will give username and password
which are as follow:
For Admin:
Username: admin
Password: admin
For User:
Username: user
Password: user
7. Now run server to serve the project by using command "python manage.py runserver"
8. After successfully running the project we can import the given postman Api collection in postman to see and test the API's
