from rest_framework import serializers 
from .models import Category, Product, Cart, Order

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields ='__all__'

class ProductSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(required=False)
    class Meta:
        model = Product
        fields ='__all__'        

class CartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cart
        fields = ['id', 'user', 'item', 'quantity', 'created_at', 'updated_at']

class OrderSerializer(serializers.ModelSerializer):
    items = serializers.JSONField()
    class Meta:
        model = Order
        fields ='__all__'