# Generated by Django 4.1.3 on 2022-12-05 03:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apis', '0018_alter_order_items_json'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='items_json',
        ),
        migrations.AddField(
            model_name='order',
            name='items_in_json',
            field=models.TextField(default={}),
            preserve_default=False,
        ),
    ]
