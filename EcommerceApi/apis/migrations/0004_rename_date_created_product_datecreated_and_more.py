# Generated by Django 4.1.3 on 2022-12-04 14:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apis', '0003_remove_product_quantity_remove_product_status'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='date_created',
            new_name='dateCreated',
        ),
        migrations.RenameField(
            model_name='product',
            old_name='image_url',
            new_name='imageUrl',
        ),
    ]
