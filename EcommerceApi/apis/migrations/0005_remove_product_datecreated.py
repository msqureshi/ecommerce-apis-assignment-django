# Generated by Django 4.1.3 on 2022-12-04 14:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apis', '0004_rename_date_created_product_datecreated_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='dateCreated',
        ),
    ]
