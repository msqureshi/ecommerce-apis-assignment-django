# Generated by Django 4.1.3 on 2022-12-05 03:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apis', '0021_remove_order_items_in_json'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='items',
        ),
    ]
