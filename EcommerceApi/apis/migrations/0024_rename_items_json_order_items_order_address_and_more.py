# Generated by Django 4.1.3 on 2022-12-05 03:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apis', '0023_order_items_json'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='items_json',
            new_name='items',
        ),
        migrations.AddField(
            model_name='order',
            name='address',
            field=models.CharField(default=1, max_length=1025),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='name',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='phone',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='pincode',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
