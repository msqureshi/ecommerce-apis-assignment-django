# Generated by Django 4.1.3 on 2022-12-04 14:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apis', '0002_product'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='quantity',
        ),
        migrations.RemoveField(
            model_name='product',
            name='status',
        ),
    ]
