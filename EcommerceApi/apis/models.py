from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
# Create your models here.

def upload_to(instance, filename):
    return 'images/{filename}'.format(filename=filename)

class Category(models.Model):
    title = models.CharField(max_length=255)

class Product(models.Model):
     title = models.CharField(max_length=255)
     category = models.ForeignKey(Category,on_delete=models.CASCADE,related_name='products')
     price = models.IntegerField()
     description = models.TextField()
     image = models.ImageField(upload_to=upload_to, blank=True, null=True)

class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=1025)
    pincode = models.IntegerField()
    phone = models.IntegerField()
    total_amount = models.IntegerField()
    items = models.TextField()


           