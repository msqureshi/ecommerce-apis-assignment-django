from django.shortcuts import render
from rest_framework import mixins, generics
from .models import Category, Product, Cart, Order
from .serializers import CategorySerializer, ProductSerializer, CartSerializer, OrderSerializer
from rest_framework.permissions import IsAuthenticated, IsAdminUser, BasePermission
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets, status
from django.contrib.auth.models import User
from .helpers import CartHelper

class WriteByAdminOnlyPermission(BasePermission):
    def has_permission(self, request, view):
        print('insidnde has permission', request.user)
        user = request.user
        if request.method == 'GET':
            return True

        if request.method == 'POST' or request.method == 'PUT' or request.method == 'DELETE':
            if user.is_superuser:
                return True

        return False

# Create your views here.
class CategoryListView(mixins.ListModelMixin,generics.GenericAPIView):    
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get(self,request):
        return self.list(request)    

class CategoryCreateView(mixins.CreateModelMixin,generics.GenericAPIView):
    permission_classes = [IsAuthenticated, WriteByAdminOnlyPermission]
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def post(self,request):
        return self.create(request)

class CategoryDetailView(mixins.RetrieveModelMixin,generics.GenericAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    def get(self,request,pk):
        return self.retrieve(request)  

class CategoryUpdateView(mixins.UpdateModelMixin,generics.GenericAPIView):
    permission_classes = [IsAuthenticated, WriteByAdminOnlyPermission]
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def put(self,request,pk):
        return self.update(request)

class CategoryDeleteView(mixins.DestroyModelMixin,generics.GenericAPIView):
    permission_classes = [IsAuthenticated, WriteByAdminOnlyPermission]
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    def delete(self,request,pk):
        return self.destroy(request)


class ProductListView(mixins.ListModelMixin,generics.GenericAPIView):    
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get(self,request):
        return self.list(request)    

class ProductCreateView(mixins.CreateModelMixin,generics.GenericAPIView):
    # permission_classes = [IsAuthenticated, WriteByAdminOnlyPermission]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    parser_classes = (MultiPartParser, FormParser)

    def post(self,request):
        return self.create(request)

class ProductDetailView(mixins.RetrieveModelMixin,generics.GenericAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    def get(self,request,pk):
        return self.retrieve(request)  

class ProductUpdateView(mixins.UpdateModelMixin,generics.GenericAPIView):
    permission_classes = [IsAuthenticated, WriteByAdminOnlyPermission]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def put(self,request,pk):
        return self.update(request)

class ProductDeleteView(mixins.DestroyModelMixin,generics.GenericAPIView):
    permission_classes = [IsAuthenticated, WriteByAdminOnlyPermission]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    def delete(self,request,pk):
        return self.destroy(request)

class CartViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

    @action(methods=['get'], detail=False, url_path='checkout/(?P<userId>[^/.]+)', url_name='checkout')
    def checkout(self, request, *args, **kwargs):

        try:
            user = User.objects.get(pk=int(kwargs.get('userId')))
        except Exception as e:
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data={'Error': str(e)})

        cart_helper = CartHelper(user)
        checkout_details = cart_helper.prepare_cart_for_checkout()
        total_amount = cart_helper.cart_base_total_amount

        if not checkout_details:
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data={'error': 'Cart of user is empty.'})

        return Response(status=status.HTTP_200_OK, data={'checkout_details': checkout_details, 'total_amount': total_amount})

class OrderPlaceView(generics.CreateAPIView):
      permission_classes = [IsAuthenticated]
      queryset = Order.objects.all()
      serializer_class = OrderSerializer

# class CreateUserView(APIView):
#     user=User.objects.create_user('user3', password='user3')
#     user.is_superuser=False
#     user.is_staff=False
#     user.save()

#     user=User.objects.create_user('admin2', password='admin2')
#     user.is_superuser=True
#     user.is_staff=True
#     user.save()








    
  
    