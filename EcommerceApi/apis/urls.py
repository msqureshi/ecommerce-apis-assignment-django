from django.contrib import admin
from django.urls import path, include
# from .views import CourseDetailView, CourseListView, InstructorDetailView, InstructorListView
from .views import CategoryListView, CategoryCreateView, CategoryDetailView, CategoryUpdateView, CategoryDeleteView, ProductListView, ProductCreateView, ProductDetailView, ProductUpdateView, ProductDeleteView, CartViewSet, OrderPlaceView
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework import routers

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

router = routers.DefaultRouter()
router.register(r'cart',CartViewSet)
urlpatterns = [
    path('category/', CategoryListView.as_view()),
    path('category/create', CategoryCreateView.as_view()),
    path('category/detail/<int:pk>', CategoryDetailView.as_view()),
    path('category/update/<int:pk>', CategoryUpdateView.as_view()),
    path('category/delete/<int:pk>', CategoryDeleteView.as_view()),

    path('product/', ProductListView.as_view()),
    path('product/create', ProductCreateView.as_view()),
    path('product/detail/<int:pk>', ProductDetailView.as_view()),
    path('product/update/<int:pk>', ProductUpdateView.as_view()),
    path('product/delete/<int:pk>', ProductDeleteView.as_view()),
    # path('courses/', CourseListView.as_view()),
    # path('courses/<int:pk>', CourseDetailView.as_view(), name='course-detail'),
    # path('instructors/<int:pk>', InstructorDetailView.as_view(),
    #      name='instructor-detail'),
    path('', include((router.urls))),
    path('place-order', OrderPlaceView.as_view()),
    # path('create-user', CreateUser),

    path('auth/login/', TokenObtainPairView.as_view(),
         name='create-token'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]