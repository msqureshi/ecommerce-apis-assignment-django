from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
class Command(BaseCommand):

    def handle(self, *args, **options):

        try:
            user=User.objects.create_user('user', password='user')
            user.is_superuser=False
            user.is_staff=False
            user.save()

            user=User.objects.create_user('admin', password='admin')
            user.is_superuser=True
            user.is_staff=True
            user.save()

            print("Users Created Please Use Username: admin, Password: admin for Admin and Username: user, Password: user for Normal User")
        except Exception as e: print(e)
   